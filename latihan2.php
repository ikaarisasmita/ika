<html>
    <head>
        <meta charset="UTF-8">
        <title>Konversi Nilai</title>
    </head>
    <body>
    <legend><h2>Konversi Nilai</h2>
        <form name="tugas_2" method="POST" action="latihan2.php">
            <table cellspacing="3" cellpadding="5">
                <tr>
                    <td>Nilai</td>
                    <th>:</th>
                    <td><input type="text" name="nilai" placeholder="Masukkan Nilai Anda"></td>
                </tr>
                <tr>
                    <th colspan="3"><input type="submit" name="konversi" value="Konversi"></th>
                </tr>
                <?php
                if(isset($_POST['konversi'])){
                    $nilai = $_POST['nilai'];
                    if ($nilai >= 91) {
                        ?><script>alert('Nilai Anda "A"');</script><?php
                    }elseif ($nilai >= 81) {
                        ?><script>alert('Nilai Anda "B"');</script><?php                       
                    }elseif ($nilai >= 61) {
                        ?><script>alert('Nilai Anda "C"');</script><?php                       
                    }elseif ($nilai >= 41) {
                        ?><script>alert('Nilai Anda "D"');</script><?php                       
                    }elseif ($nilai <= 0) {
                        ?><script>alert('Nilai Anda "E"');</script><?php                       
                    } else{
                        ?><script>alert('Maaf data yang anda masukkan salah!');</script><?php                                              
                    }
                }
                ?>
            </table>
        </form>       
    </legend>
        <?php
        ?>
    </body>
</html>